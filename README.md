# SAN HTML

A simple app for producing HTML and SASS for the SAN project.

## Get it running

* Clone this repo
* `npm install`
* `npm run watch:css`
* `npm run watch:html`
* `python -m SimpleHTTPServer 8000`

Browse to `http://localhost:8000/public`.