window.onload = () => {
    const attractor = new SanAttractorAnimation(window, 10);

    let isAttractorPlaying = false;

    // attractor.start();

    document.addEventListener('click', () => {
        if(isAttractorPlaying){
            attractor.stop();
        } else {
            attractor.start();
        }

        isAttractorPlaying = !isAttractorPlaying;
    });
};		