'user strict';

/**
 * A component for animating the SAN video / transparent Attractor.
 * Depends on GSAP TweenMax library.
 *
 * @class TransparentAttractorAnimation
 */

class SanAttractorAnimation {

	constructor(window, videoInterval=5){
		// get attractor element
		this.attractorDiv = window.document.getElementById('attractor');
		// get media element
		this.media = this.attractorDiv.querySelector('video');
		this.media.style.opacity = 0;

		// circle pulse animation
		const circleDiv = this.attractorDiv.querySelector('.circles');

		const circleTl = new TimelineMax({
			yoyo: true,
			repeat: -1
		});

		circleTl.to(circleDiv, 2, {scale: 1.3, opacity: 0.25});

		// hand wobble animation
		const handDiv = this.attractorDiv.querySelector('.hand');
		const handTl = new TimelineMax({
			repeat: -1
		});
		handTl.delay(2)
			  .to(handDiv, 0, {rotationZ: 0})
			  .to(handDiv, 0.03, {rotationZ: 20, ease: Elastic.easeInOut})
			  .to(handDiv, 0.03, {rotationZ: 0})
			  .to(handDiv, 0.03, {rotationZ: -20, ease: Elastic.easeInOut})
			  .to(handDiv, 0.02, {rotationZ: 0}, '+=0.1')
			  .to(handDiv, 0.03, {rotationZ: 20, ease: Elastic.easeInOut})
			  .to(handDiv, 0.02, {rotationZ: 0})
			  .to(handDiv, 0.03, {rotationZ: -20})
			  .to(handDiv, 0.02, {rotationZ: 0})
			  .to(handDiv, 0, {rotationZ: 0},'+=5')
			  ;

		// main overlay timeline
		this.overlayTl = new TimelineMax({
			repeat: -1
		});
		this.overlayTl.add(circleTl)
				   .add(handTl)
				   ;

		// video timeline
		this.videoTl = new TimelineMax({
			repeat: -1
		});

		this.videoTl.to(this.media, 0, {opacity: 1})
		   .add(() => { this._startVideo(); }, `+=${videoInterval}`) 
		   .add(() => { this._pauseVideo(); } , `+=${videoInterval}`)
		   ;

		this.stop();
	}

	start() {
		// unhide attractor
		TweenMax.to(this.attractorDiv, 0.5, {opacity: 1});

		// start animation
		this._startAnimation();
		this.videoTl.restart();
	}

	stop() {
		TweenMax.to(this.attractorDiv, 0.1, {opacity: 0});
		this._stopAnimation();
		this.videoTl.pause();
		this._stopVideo();
	}

	/**
	 * "private" methods
	 * TODO: make these methods private
	 */

	_startAnimation() {
		this.overlayTl.restart();
	}

	_stopAnimation() {
		this.overlayTl.pause();
	}

	_startVideo() {
		this.media.play();
		TweenMax.to(this.media, 2, {opacity: 1});
	}

	_pauseVideo() {
		TweenMax.to(this.media, 1, {opacity: 0});
		this.media.pause();
	}

	_stopVideo(){    
		this.media.pause();
		// reset to beginning  
		this.media.currentTime = 0;
		TweenMax.to(this.media, 0.5, {opacity: 0});

	}

}